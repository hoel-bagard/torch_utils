import multiprocessing as mp
from multiprocessing import shared_memory
import os
from typing import Callable, Iterable, Optional

import h5py
import numpy as np

try:
    from .batch_generator import BatchGenerator
except ImportError:  # If running this script directly
    from batch_generator import BatchGenerator


class SequenceGenerator(BatchGenerator):

    def __init__(self, data: Iterable, label: Iterable, sequence_size: int, batch_size: int,
                 data_processor: Optional[Callable] = None, label_processor: Optional[Callable] = None,
                 prefetch=True, preload=False, num_workers=1, shuffle=True, initial_shuffle=False,
                 flip_data=False, save: Optional[str] = None):
        self.batch_size = batch_size
        self.sequence_size = sequence_size
        self.shuffle = shuffle
        self.prefetch = prefetch and not preload
        self.num_workers = num_workers
        self.flip_data = flip_data

        if not preload:
            self.data_processor = data_processor
            self.label_processor = label_processor
            self.data = np.asarray(data)
            self.label = np.asarray(label)
        else:
            self.data_processor = None
            self.label_processor = None
            save_path = save
            if save is not None:
                if '.' not in os.path.basename(save_path):
                    save_path += '.hdf5'
                if not os.path.exists(os.path.dirname(save_path)):
                    os.makedirs(os.path.dirname(save_path))

            if save and os.path.exists(save_path):
                with h5py.File(save_path, 'r') as h5_file:
                    data_len = np.asarray(h5_file['data_len'])
                    self.data = []
                    self.label = []
                    for sequence_index in range(data_len):
                        self.data.append(np.asarray(h5_file[f'data_{sequence_index}']))
                        self.label.append(np.asarray(h5_file[f'label_{sequence_index}']))
                    self.data = np.asarray(self.data)
                    self.label = np.asarray(self.label)
            else:
                if data_processor:
                    self.data = np.asarray(
                        [np.asarray([data_processor(entry) for entry in serie]) for serie in data],
                        dtype=np.object if len(data) > 1 else None)
                else:
                    self.data = np.asarray(data)
                if label_processor:
                    self.label = np.asarray(
                        [np.asarray([label_processor(entry) for entry in serie]) for serie in label],
                        dtype=np.object if len(label) > 1 else None)
                else:
                    self.label = np.asarray(label)
                if save:
                    with h5py.File(save_path, 'w') as h5_file:
                        h5_file.create_dataset('data_len', data=len(self.data))
                        for sequence_index in range(len(self.data)):
                            h5_file.create_dataset(f'data_{sequence_index}', data=self.data[sequence_index])
                            h5_file.create_dataset(f'label_{sequence_index}', data=self.label[sequence_index])

        self.index_list = []
        for sequence_index in range(len(self.data)):
            start_indices = np.expand_dims(
                np.arange(len(self.data[sequence_index]) - sequence_size + 1, dtype=np.uint32),
                axis=-1)
            start_indices = np.insert(start_indices, 0, sequence_index, axis=1)
            self.index_list.append(start_indices)
        self.index_list = np.concatenate(self.index_list, axis=0)
        if shuffle or initial_shuffle:
            np.random.shuffle(self.index_list)
        self.step_per_epoch = len(self.index_list) // self.batch_size
        self.last_batch_size = len(self.index_list) % self.batch_size
        if self.last_batch_size == 0:
            self.last_batch_size = self.batch_size

        self.epoch = 0
        self.global_step = 0
        self.step = 0

        if data_processor:
            first_data = []
            for sequence_index, start_index in self.index_list[:batch_size]:
                first_data.append(
                    [data_processor(input_data)
                        for input_data in self.data[sequence_index][start_index: start_index + self.sequence_size]])
            first_data = np.asarray(first_data)
        else:
            first_data = []
            for sequence_index, start_index in self.index_list[:batch_size]:
                first_data.append(
                    self.data[sequence_index][start_index: start_index + self.sequence_size])
            first_data = np.asarray(first_data)
        if label_processor:
            first_label = []
            for sequence_index, start_index in self.index_list[:batch_size]:
                first_label.append(
                    [label_processor(input_label)
                        for input_label in self.label[sequence_index][start_index: start_index + self.sequence_size]])
            first_label = np.asarray(first_label)
        else:
            first_label = []
            for sequence_index, start_index in self.index_list[:batch_size]:
                first_label.append(
                    self.label[sequence_index][start_index: start_index + self.sequence_size])
            first_label = np.asarray(first_label)
        self.batch_data = first_data
        self.batch_label = first_label

        self.process_id = 'NA'
        if self.prefetch or self.num_workers > 1:
            self.cache_memory_indices = shared_memory.SharedMemory(create=True, size=self.index_list.nbytes)
            self.cache_indices = np.ndarray(
                self.index_list.shape, dtype=self.index_list.dtype, buffer=self.cache_memory_indices.buf)
            self.cache_indices[:] = self.index_list
            self.cache_memory_data = [
                shared_memory.SharedMemory(create=True, size=first_data.nbytes),
                shared_memory.SharedMemory(create=True, size=first_data.nbytes)]
            self.cache_data = [
                np.ndarray(first_data.shape, dtype=first_data.dtype, buffer=self.cache_memory_data[0].buf),
                np.ndarray(first_data.shape, dtype=first_data.dtype, buffer=self.cache_memory_data[1].buf)]
            self.cache_memory_label = [
                shared_memory.SharedMemory(create=True, size=first_label.nbytes),
                shared_memory.SharedMemory(create=True, size=first_label.nbytes)]
            self.cache_label = [
                np.ndarray(first_label.shape, dtype=first_label.dtype, buffer=self.cache_memory_label[0].buf),
                np.ndarray(first_label.shape, dtype=first_label.dtype, buffer=self.cache_memory_label[1].buf)]
        else:
            self.cache_memory_indices = None
            self.cache_data = [first_data]
            self.cache_label = [first_label]

        if self.prefetch:
            self.prefetch_pipe_parent, self.prefetch_pipe_child = mp.Pipe()
            self.prefetch_stop = shared_memory.SharedMemory(create=True, size=1)
            self.prefetch_stop.buf[0] = 0
            self.prefetch_skip = shared_memory.SharedMemory(create=True, size=1)
            self.prefetch_skip.buf[0] = 0
            self.prefetch_process = mp.Process(target=self._prefetch_worker)
            self.prefetch_process.start()
            self.num_workers = 0
        self._init_workers()
        self.current_cache = 0
        self.process_id = 'main'

    def _next_batch(self):
        if self.num_workers > 1:
            self._worker_next_batch()
            return

        # Loading data
        if self.data_processor is not None:
            data = []
            for sequence_index, start_index in self.index_list[
                    self.step * self.batch_size:(self.step + 1) * self.batch_size]:
                data.append(
                    [self.data_processor(input_data)
                        for input_data in self.data[sequence_index][start_index: start_index + self.sequence_size]])
            if self.flip_data:
                flip = np.random.uniform()
                if flip < 0.25:
                    self.cache_data[self.current_cache][:len(data)] = np.asarray(data)[:, :, :, ::-1]
                elif flip < 0.5:
                    self.cache_data[self.current_cache][:len(data)] = np.asarray(data)[:, :, :, :, ::-1]
                elif flip < 0.75:
                    self.cache_data[self.current_cache][:len(data)] = np.asarray(data)[:, :, :, ::-1, ::-1]
                else:
                    self.cache_data[self.current_cache][:len(data)] = np.asarray(data)
            else:
                self.cache_data[self.current_cache][:len(data)] = np.asarray(data)
        else:
            data = []
            for sequence_index, start_index in self.index_list[
                    self.step * self.batch_size:(self.step + 1) * self.batch_size]:
                data.append(self.data[sequence_index][start_index: start_index + self.sequence_size])
            if self.flip_data:
                flip = np.random.uniform()
                if flip < 0.25:
                    self.cache_data[self.current_cache][:len(data)] = np.asarray(data)[:, :, :, ::-1]
                elif flip < 0.5:
                    self.cache_data[self.current_cache][:len(data)] = np.asarray(data)[:, :, :, :, ::-1]
                elif flip < 0.75:
                    self.cache_data[self.current_cache][:len(data)] = np.asarray(data)[:, :, :, ::-1, ::-1]
                else:
                    self.cache_data[self.current_cache][:len(data)] = np.asarray(data)
            else:
                self.cache_data[self.current_cache][:len(data)] = np.asarray(data)

        # Loading label
        if self.label_processor is not None:
            label = []
            for sequence_index, start_index in self.index_list[
                    self.step * self.batch_size:(self.step + 1) * self.batch_size]:
                label.append(
                    [self.label_processor(input_data)
                     for input_data in self.label[sequence_index][start_index: start_index + self.sequence_size]])
            self.cache_label[self.current_cache][:len(label)] = np.asarray(label)
        else:
            label = []
            for sequence_index, start_index in self.index_list[
                    self.step * self.batch_size:(self.step + 1) * self.batch_size]:
                label.append(self.label[sequence_index][start_index: start_index + self.sequence_size])
            self.cache_label[self.current_cache][:len(label)] = np.asarray(label)


if __name__ == '__main__':
    def test():
        data = np.array(
            [[1, 2, 3, 4, 5, 6, 7, 8, 9], [11, 12, 13, 14, 15, 16, 17, 18, 19]], dtype=np.uint8)
        label = np.array(
            [[.1, .2, .3, .4, .5, .6, .7, .8, .9], [.11, .12, .13, .14, .15, .16, .17, .18, .19]], dtype=np.uint8)

        for data_processor in [None, lambda x:x]:
            for prefetch in [False, True]:
                for num_workers in [1, 2]:
                    print(f'{data_processor=} {prefetch=} {num_workers=}')
                    with SequenceGenerator(data, label, 5, 2, data_processor=data_processor,
                                           prefetch=prefetch, num_workers=num_workers) as batch_generator:
                        for _ in range(9):
                            print(batch_generator.batch_data, batch_generator.epoch, batch_generator.step)
                            batch_generator.next_batch()
                    print()

    test()
