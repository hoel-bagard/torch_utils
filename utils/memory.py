def human_size(byte_count: int) -> str:
    """Output byte amount in human readable format"""
    amount = float(byte_count)
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi', 'Yi']:
        if amount < 1024.0:
            break
        amount /= 1024.0
    return f'{amount:.2f}{unit}B'
